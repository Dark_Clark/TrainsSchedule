﻿using System.Windows;

namespace Bordushko.TrainsSchedule.Views
{
    /// <summary>
    /// Interaction logic for RemoveWindow.xaml
    /// </summary>
    public partial class RemoveWindow : Window
    {
        public RemoveWindow()
        {
            InitializeComponent();
        }
    }
}
